<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {

    private const TABLE_NAME = 'slide_additional_images';

    public function up(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->dropForeign('slide_additional_images_slide_id_foreign');
            $table->foreign('slide_id')->references('id')->on('sliders_slides')->onDelete('cascade');
        });
    }

    public function down(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->dropForeign('slide_additional_images_slide_id_foreign');
            $table->foreign('slide_id')->references('id')->on('sliders_slides');
        });
    }
};
