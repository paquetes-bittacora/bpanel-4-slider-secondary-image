<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {

    private const TABLE_NAME = 'slide_additional_images';

    public function up(): void
    {
        Schema::create(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->id();
            $table->unsignedBigInteger('slide_id');
            $table->timestamps();

            $table->foreign('slide_id')->references('id')->on('sliders_slides');
        });
    }

    public function down(): void
    {
        Schema::drop(self::TABLE_NAME);
    }
};
