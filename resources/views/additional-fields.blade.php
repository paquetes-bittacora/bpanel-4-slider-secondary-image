@php
    use Bittacora\Bpanel4\Slider\Models\Slide;
    use Bittacora\Bpanel4\SliderAdditionalImage\Models\SlideAdditionalImage;
    /** @var Slide $slide */

    $additionalImage = (new SlideAdditionalImage())->where('slide_id', $slide?->getId())->first();
@endphp
@if (null !== $additionalImage?->getImage())
    <div class="form-group form-row">
        <div class="col-sm-3 col-form-label text-sm-right">
            <label for="id-form-field-1" class="mb-0  ">
                Imagen secundaria actual
            </label>
        </div>

        <div class="col-sm-7">
            <img src="{{ $additionalImage?->getImage()->getFullUrl() }}" style="max-width: 500px;">
            <div class="mt-2">
                <a onclick="return confirm('¿Confirma que desea quitar la imagen?')" href="{{ route('bpanel4-slider-additional-image.bpanel.remove-additional-image', ['slide' => $slide]) }}" class="btn btn-danger"><i class="fas fa-times"></i> Quitar</a>
            </div>
        </div>
    </div>
@endif
@livewire('form::input-file', ['name' => 'secondary_image', 'labelText' => 'Imagen secundaria',
'maxFileCount' => 1, 'accept' => '.jpg,.jpeg,.png,.webp', 'allowedFileExtensions' => 'jpg,jpeg,png,webp' ])
