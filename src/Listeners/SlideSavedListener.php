<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\SliderAdditionalImage\Listeners;

use Bittacora\Bpanel4\Slider\Events\SlideSaved;
use Bittacora\Bpanel4\SliderAdditionalImage\Models\SlideAdditionalImage;
use Illuminate\Http\UploadedFile;

final class SlideSavedListener
{
    public function handle(SlideSaved $event): void
    {
        $additionalImage = (new SlideAdditionalImage())->where('slide_id', $event->slide->getId())->first();

        if (null === $additionalImage) {
            $additionalImage = new SlideAdditionalImage();
            $additionalImage->slide_id = $event->slide->getId();
            $additionalImage->save();
        }

        $file = $event->request->file('secondary_image');

        if (!$file instanceof UploadedFile) {
            return;
        }

        $additionalImage->clearMediaCollection('additional_image');

        $additionalImage->addMedia($file)->toMediaCollection('additional_image', 'images');
    }
}