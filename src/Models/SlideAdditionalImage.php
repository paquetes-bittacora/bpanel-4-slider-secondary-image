<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\SliderAdditionalImage\Models;

use Bittacora\Bpanel4\Slider\Models\Slide;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

final class SlideAdditionalImage extends Model implements HasMedia
{
    use InteractsWithMedia;

    public function slide(): BelongsTo
    {
        return $this->belongsTo(Slide::class);
    }

    public function getImage(): ?Media
    {
        return $this->getMedia('additional_image')->first();
    }
}