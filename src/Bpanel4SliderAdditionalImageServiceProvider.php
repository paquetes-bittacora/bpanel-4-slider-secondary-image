<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\SliderAdditionalImage;

use Bittacora\Bpanel4\HooksComponent\Services\Hooks;
use Bittacora\Bpanel4\Slider\Events\SlideSaved;
use Bittacora\Bpanel4\Slider\Models\Slide;
use Bittacora\Bpanel4\SliderAdditionalImage\Listeners\SlideSavedListener;
use Bittacora\Bpanel4\SliderAdditionalImage\Models\SlideAdditionalImage;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\ServiceProvider;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

final class Bpanel4SliderAdditionalImageServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'bpanel4-slider-additional-image';

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function boot(Hooks $hook, Dispatcher $dispatcher): void
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->registerHooks($hook);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');

        $dispatcher->listen(SlideSaved::class, SlideSavedListener::class);
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function registerHooks(Hooks $hook): void
    {
        /** @var Factory $blade */
        $blade = $this->app->get('view');

        $hook->register('slide-additional-fields', function ($slide) use ($blade): void {
            echo $blade->make(self::PACKAGE_PREFIX . '::additional-fields', ['slide' => $slide]);
        });

        $hook->register('show-slide-additional-fields', function (Slide $slide) use ($blade): void {
            $secondaryImage = (new SlideAdditionalImage())->where('slide_id', $slide?->getId())->first();

            if (null !== $secondaryImage) {
                echo $blade->make(self::PACKAGE_PREFIX . '::show-additional-fields', [
                    'secondaryImage' => $secondaryImage
                ]);
            }
        });
    }
}