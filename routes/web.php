<?php

declare(strict_types=1);

use Bittacora\Bpanel4\Slider\Models\Slide;
use Bittacora\Bpanel4\SliderAdditionalImage\Models\SlideAdditionalImage;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel/sliders')->name('bpanel4-slider-additional-image.bpanel.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(function (): void {
        Route::get('/diapositiva/{slide}/quitar-imagen-adicional', function (Slide $slide) {
            $secondaryImage = (new SlideAdditionalImage())->where('slide_id', $slide?->getId())->first();
            $secondaryImage?->delete();
            return back()->with('alert-success', 'Imagen eliminada');
        })->name('remove-additional-image');
    });
